package oop.asg04;

// Board.java
//Nguyen Van Duc
import java.util.Arrays;

public class Board {

    private int width;
    private int height;
    private boolean[][] grid;
    private boolean DEBUG = true;
    boolean committed;
    // Khai bao them 1 so bien
    private int[] widths; //mang chua so cac khoi da duoc lap day cua moi hang
    private int[] heights; // chua cac chieu cao cua moi cot
    private int maxHeight;
    private boolean[][] bGrid; //mang phu dung de back up grid,widths,heights,maxHeight
    private int[] bWidths;
    private int[] bHeights;
    private int bMaxHeight;

    public Board(int width, int height) {
        this.width = width;
        this.height = height;
        grid = new boolean[width][height];
        committed = true;

        //Khoi tao mang Grid
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                grid[i][j] = false;
            }
        }

        // Khoi tao mang widths va heights
        this.widths = new int[height];
        this.heights = new int[width];
        Arrays.fill(this.widths, 0);
        Arrays.fill(this.heights, 0);
        this.bGrid = new boolean[width][height];

        this.bWidths = new int[height];
        this.bHeights = new int[width];

        this.maxHeight = 0;
        this.bMaxHeight = 0;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    // cai dat mang widths
    private int[] setWidths() {
        for (int i = 0; i < this.height; i++) {
            int counter = 0;
            for (int j = 0; j < this.width; j++) {
                if (this.grid[j][i] == true) {
                    counter++;
                }
            }
            this.widths[i] = counter;
        }
        return this.widths;
    }
    //cai dat mang heights

    private int[] setHeights() {
        for (int i = 0; i < this.width; i++) {
            int counter = 0;
            for (int j = this.height - 1; j >= 0; j--) {
                if (grid[i][j] == true) {
                    counter = j + 1;
                    break;
                }
            }
            this.heights[i] = counter;
        }
        return this.heights;
    }

    public int getMaxHeight() {
        int max = heights[0];
        for (int i = 0; i < this.width; i++) {
            if (max < this.heights[i]) {
                max = heights[i];
            }
        }
        return max;
    }

    //BackUp dataBase
    private void backUp() {
        System.arraycopy(this.heights, 0, this.bHeights, 0, this.width);
        System.arraycopy(widths, 0, bWidths, 0, height);
        this.bMaxHeight = this.maxHeight;
        for (int h = 0; h < height; h++) {
            for (int w = 0; w < width; w++) {
                bGrid[w][h] = grid[w][h];
            }
        }
    }

    public void sanityCheck() {
        if (DEBUG) {
            for (int y = 0; y < this.height; y++) {
                int filled = 0;
                for (int x = 0; x < this.width; x++) {
                    if (this.grid[x][y]) {
                        filled++;
                    }
                }
                if (this.widths[y] != filled) {
                    throw new RuntimeException("Widths KHONG DUNG!");
                }
            }
            for (int x = 0; x < this.width; x++) {
                int high = 0;
                for (int y = 0; y < this.height; y++) {
                    if (this.grid[x][y]) {
                        high = y + 1;
                    }
                }
                if (this.heights[x] != high) {
                    throw new RuntimeException("Heights KHONG DUNG!");
                }
            }
            int max = 0;
            for (int i = 0; i < this.width; i++) {
                max = Math.max(heights[i], max);
            }
            if (max != maxHeight) {
                throw new RuntimeException("Max height KHONG DUNG!");
            }
        }
    }

    public int dropHeight(Piece piece, int x) {
        int maxHeight = 0;
        int[] skirt = piece.getSkirt();
        for (int i = 0; i < skirt.length; i++) {
            int colDropHeight = heights[x + i] - skirt[i];
            maxHeight = Math.max(colDropHeight, maxHeight);
        }
        return maxHeight;
    }

    public int getColumnHeight(int x) {
        return heights[x];
    }

    public int getRowWidth(int y) {
        return widths[y];
    }

    public boolean getGrid(int x, int y) {
        if (x > width || y > height) {
            return true;
        } else {
            return grid[x][y];
        }
    }
    public static final int PLACE_OK = 0;
    public static final int PLACE_ROW_FILLED = 1;
    public static final int PLACE_OUT_BOUNDS = 2;
    public static final int PLACE_BAD = 3;

    public int place(Piece piece, int x, int y) {
        if (!committed) {
            throw new RuntimeException("place commit problem");
        }

        committed = false;
        backUp();
        int result = PLACE_OK;
        TPoint[] body = piece.getBody();
        for (int i = 0; i < body.length; i++) {
            int a = x + body[i].x;
            int b = y + body[i].y;
            if (x + piece.getWidth() > this.width || y + piece.getHeight() > this.height
                    || x < 0 || y < 0) {
                result = PLACE_OUT_BOUNDS;
                undo();
                break;
            } else if (this.grid[a][b] == true) {
                result = PLACE_BAD;
                undo();
                break;
            } else {
                this.grid[a][b] = true;
                this.widths[b] += 1;
                if (this.widths[b] == this.width) {
                    result = PLACE_ROW_FILLED;
                }
                if (this.heights[a] < b + 1) {
                    this.heights[a] = b + 1;
                }
                if (heights[a] > this.maxHeight) {
                    this.maxHeight = heights[a];
                }
            }
        }
        if (result <= PLACE_ROW_FILLED) {
            sanityCheck();
        }
        return result;
    }

    public void undo() {
        // YOUR CODE HERE
        if (committed == false) {
            boolean[][] temp = this.grid;
            grid = this.bGrid;
            this.bGrid = temp;

            int[] temphw = this.widths;
            this.widths = bWidths;
            this.bWidths = temphw;

            temphw = this.heights;
            this.heights = bHeights;
            bHeights = temphw;

            int tempmax = this.maxHeight;
            this.maxHeight = bMaxHeight;
            this.bMaxHeight = tempmax;

            committed = true;
        }
        sanityCheck();
    }

    public int clearRows() {
        committed = false;
        int rowsCleared = 0;
        for (int i = this.getMaxHeight() - 1; i >= 0; i--) {
            if (this.widths[i] == this.width) {
                rowsCleared = rowsCleared + 1;
                for (int t = i; t < this.getMaxHeight(); t = t + 1) {
                    if (t < this.height - 1) {
                        for (int j = 0; j < this.width; j++) {
                            this.grid[j][t] = this.grid[j][t + 1];
                        }
                    } else {
                        for (int j = 0; j < this.width; j++) {
                            this.grid[j][t] = false;
                        }
                    }
                }
            }
        }
        this.heights = setHeights();
        this.widths = setWidths();
        this.maxHeight = getMaxHeight();
        committed = false;
        sanityCheck();
        return rowsCleared;
    }

    public void commit() {
        committed = true;
//        backUp();
    }

    public String toString() {
        StringBuilder buff = new StringBuilder();
        for (int y = height - 1; y >= 0; y--) {
            buff.append('|');
            for (int x = 0; x < width; x++) {
                if (getGrid(x, y)) {
                    buff.append('+');
                } else {
                    buff.append(' ');
                }
            }
            buff.append("|\n");
        }
        for (int x = 0; x < width + 2; x++) {
            buff.append('-');
        }
        return (buff.toString());
    }
}
