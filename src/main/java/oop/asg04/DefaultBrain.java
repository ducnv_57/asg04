package oop.asg04;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author giang
 */
// DefaultBrain.java

/**
 Provided code.
 A simple Brain implementation.
 bestMove() iterates through all the possible x values
 and rotations to play a particular piece (there are only
 around 10-30 ways to play a piece).
 M? cung c?p s?n.
 M?t c�i �?t ��n gi?n c?a Brain
 bestMove() duy?t t?t c? c�c gi� tr? x v� c�c tr?ng th�i c� th? �? t?m c�ch ch�i hi?u qu? nh?t cho m?t m?nh c? th? (ch? c� kho?ng 10-30 c�ch ch�i m?t m?nh).
 
 For each play, it uses the rateBoard() message to rate how
 good the resulting board is and it just remembers the
 play with the lowest score. Undo() is used to back-out
 each play before trying the next. To experiment with writing your own
 brain -- just subclass off DefaultBrain and override rateBoard().
 M?i c�ch ch�i, n� d�ng h�m rateBoard() �? ��nh gi� b?ng k?t qu? v� ch? ghi nh? c�ch ch�i ra �i?m th?p nh?t. D�ng undo() �? x�a m?i c�ch ch�i tr�?c khi th? c�ch ti?p theo. �? th� nghi?m v?i vi?c t? vi?t brain, b?n ch? c?n vi?t l?p con c?a DefaultBrain v� c�i �� rateBoard().
*/

public class DefaultBrain implements Brain {
    /**
     Given a piece and a board, returns a move object that represents
     the best play for that piece, or returns null if no play is possible.
     See the Brain interface for details.
    */
    public Brain.Move bestMove(Board board, Piece piece, int limitHeight, Brain.Move move) {
        // Allocate a move object if necessary
        if (move==null) move = new Brain.Move();
        
        double bestScore = 1e20;
        int bestX = 0;
        int bestY = 0;
        Piece bestPiece = null;
        Piece current = piece;
        
        board.commit();
        
        // loop through all the rotations
        while (true) {
            final int yBound = limitHeight - current.getHeight()+1;
            final int xBound = board.getWidth() - current.getWidth()+1;
            
            // For current rotation, try all the possible columns
            for (int x = 0; x<xBound; x++) {
                int y = board.dropHeight(current, x);
                if (y<yBound) {    // piece does not stick up too far
                    int result = board.place(current, x, y);
                    if (result <= Board.PLACE_ROW_FILLED) {
                        if (result == Board.PLACE_ROW_FILLED) board.clearRows();
                        
                        double score = rateBoard(board);
                        
                        if (score<bestScore) {
                            bestScore = score;
                            bestX = x;
                            bestY = y;
                            bestPiece = current;
                        }
                    }
                    
                    board.undo();    // back out that play, loop around for the next
                }
            }
            
            current = current.fastRotation();
            if (current == piece) break;    // break if back to original rotation
        }
        
        if (bestPiece == null) return(null);    // could not find a play at all!
        else {
            move.x = bestX;
            move.y = bestY;
            move.piece = bestPiece;
            move.score = bestScore;
            return(move);
        }
    }
    
    
    /*
     A simple brain function.
     Given a board, produce a number that rates
     that board position -- larger numbers for worse boards.
     This version just counts the height
     and the number of "holes" in the board.
	 
	 Ch?c n�ng t�nh to�n ��n gi?n.
	 Cho m?t b?ng, t�nh m?t gi� tr? ��nh gi� �? t?t c?a b?ng
	 gi� tr? c�ng l?n th? b?ng c�ng t?i.
	 Phi�n b?n n�y ch? �?m �? cao v� s? "l?" trong b?ng.
    */
    public double rateBoard(Board board) {
        final int width = board.getWidth();
        final int maxHeight = board.getMaxHeight();
        
        int sumHeight = 0;
        int holes = 0;
        // MY EXTRA VARIABLE
        int badHole = 0;;
        int slit = 0;
        //two are my own ^
        // Count the holes, and sum up the heights
        for (int x=0; x<width; x++) {
            final int colHeight = board.getColumnHeight(x);
            sumHeight += colHeight;
            if (x == 0){
                if (board.getColumnHeight(x+1) - colHeight >= 3)
                    slit += 30;
            }
            else if (x == width - 1){
                if (board.getColumnHeight(x-1) - colHeight >= 3)
                    slit += 30;
            }
            else {
                if (board.getColumnHeight(x-1) - colHeight >= 3 && board.getColumnHeight(x+1) - colHeight >= 3) 
                    slit += 30;
            }
            int y = colHeight - 2;    // addr of first possible hole
            
            while (y>=0) {
                if  (!board.getGrid(x,y)) {
                    holes++;
//MY EXTRA CODE HERE
                    int tmp = y - 1;
                    while (tmp >= 1){
                        if (!board.getGrid(x, tmp)){
                            badHole += 10;
                            y--;
                        }
                        tmp--;
                    }
//END
                }
                y--;
            }
        }
        
        double avgHeight = ((double)sumHeight)/width;
        
        // Add up the counts to make an overall score
        // The weights, 8, 40, etc., are just made up numbers that appear to work
        return (8*maxHeight + 40*avgHeight + 1.25*holes + badHole + slit);    
    }
    public Piece trollPiece  (Board board){
        Piece[] pieces = Piece.getPieces();
        double[] piecesScore = new double[7];
        for (int i = 0; i < 7; i++){
            Brain.Move move = new Move();
            move = bestMove(board, pieces[i], board.getHeight(), move);
            piecesScore[i] = move.score;
        }
        int max = 0;
        double maxValue = piecesScore[0];
        for (int i = 1; i < 7; i++){
            if (piecesScore[i] > maxValue){
                maxValue = piecesScore[i];
                max = i;
            }
        }
        return pieces[max];
    }
}
