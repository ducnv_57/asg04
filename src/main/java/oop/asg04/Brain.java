package oop.asg04;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author giang
 */
// Brain.java -- the interface for Tetris brains

public interface Brain {
    // Move is used as a struct to store a single Move
    // ("static" here means it does not have a pointer to an
    // enclosing Brain object, it's just in the Brain namespace.)
	// Move l� c?u tr�c l�u m?t n�?c ��n.
	// ("static" ? ��y c� ngh?a r?ng n� kh�ng c� m?t con tr? 
	// t?i m?t �?i t�?ng Brain b?c ngo�i, 
	// n� ch? n?m trong kh�ng gian t�n Brain.)
    public static class Move {
        public int x;
        public int y;
        public Piece piece;
        public double score;    // lower scores are better
    }
    
    /**
     Given a piece and a board, returns a move object that represents
     the best play for that piece, or returns null if no play is possible.
     The board should be in the committed state when this is called.
	 Cho m?t m?nh v� m?t b?ng, tr? v? m?t �?i t�?ng move 
	 �?i di?n cho n�?c �i t?t nh?t cho m?nh ��, 
	 ho?c tr? v? null n?u kh�ng th? ch�i m?nh ��.
	 B?ng c?n ? tr?ng th�i committed khi g?i h�m n�y.
     
     limitHeight is the height of the lower part of the board that pieces
     must be inside when they land for the game to keep going
      -- typically 20 (i.e. board.getHeight() - 4)
     If the passed in move is non-null, it is used to hold the result
     (just to save the memory allocation).
    */
    public Brain.Move bestMove(Board board, Piece piece, int limitHeight, Brain.Move move);
}