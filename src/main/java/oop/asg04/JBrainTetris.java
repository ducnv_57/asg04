package oop.asg04;
//Nguyen Van Duc
import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.event.*;
import java.awt.Toolkit;

public class JBrainTetris extends JTetris {

    private static final long serialVersionUID = 1L;

    public JBrainTetris(int pixels) {
        super(pixels);
    }
    protected JCheckBox brainMode;
    protected JLabel status;
    protected JSlider adversary;
    private static DefaultBrain brain;
    private static int braincheck;
    private Brain.Move best;

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ignored) {
        }

        JBrainTetris tetris = new JBrainTetris(16);
        JFrame frame = JTetris.createFrame(tetris);
        frame.setVisible(true);
        braincheck = 0;
        brain = new DefaultBrain();
    }

    @Override
    public JComponent createControlPanel() {
        JPanel panel = (JPanel) super.createControlPanel();
        panel.add(new JLabel("Brain:"));
        this.brainMode = new JCheckBox("Brain Active");
        panel.add(this.brainMode);

        JPanel little = new JPanel();
        little.add(new JLabel("Adversary:"));
        adversary = new JSlider(0, 100, 0);    // min, max, current
        this.adversary.setPreferredSize(new Dimension(100, 15));
        little.add(this.adversary);
        panel.add(little);

        status = new JLabel("Ok");
        panel.add(this.status);
        return panel;
    }

    @Override
    public Piece pickNextPiece() {
        Random rgen = new Random();
        int pieceNum = rgen.nextInt(99) + 1;
        if (pieceNum < adversary.getValue()) {
            this.status.setText("*Ok*");
            Piece adPiece = pieces[0];
            this.best = this.brain.bestMove(board, adPiece, board.getHeight(), best);
            double badScore = this.best.score;
            double testScore = this.best.score;
            for (Piece piece : pieces) {
                this.best = brain.bestMove(board, piece, board.getHeight(), this.best);
                testScore = this.best.score;
                if (testScore > badScore) {
                    badScore = testScore;
                    adPiece = piece;
                }
            }
            return adPiece;
        } else {
            status.setText("Ok");
            return super.pickNextPiece();
        }
    }

    @Override
    public void tick(int verb) {
        if (this.brainMode.isSelected()) {
            if (braincheck != count) {
                board.undo();
                this.best = brain.bestMove(board, currentPiece, board.getHeight(), best);
                this.braincheck = count;
            }
            if (!currentPiece.equals(this.best.piece)) {
                super.tick(ROTATE);
            } else if (currentX > best.x) {
                super.tick(LEFT);
            } else if (currentX < best.x) {
                super.tick(RIGHT);
            }
            super.tick(verb);
        } else {
            super.tick(verb);
        }
    }
}