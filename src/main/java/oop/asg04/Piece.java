package oop.asg04;

import java.util.*;

public class Piece {
//Nguyen Van Duc
    private TPoint[] body;
    public int[] skirt;
    private int width;
    private int height;
    private Piece next; 
    static private Piece[] pieces;

    public Piece(TPoint[] points) {
        // YOUR CODE HERE
        this.body = new TPoint[points.length];
        System.arraycopy(points, 0, body, 0, points.length);
        width = -1;
        height = -1;

        for (int i = 0; i < 4; i=i+1) {
            width = Math.max(width, points[i].x);
            height = Math.max(height, points[i].y);
        }
        ++this.width;
        ++this.height;
        skirt = new int[this.width];
        for (int i = 0; i < width; i++) {
            this.skirt[i] = this.height;
        }
        for (int i = 0; i < 4; i++) {
            skirt[points[i].x] = Math.min(this.skirt[points[i].x], points[i].y);
        }
    }

    public Piece(String points) {
        this(parsePoints(points));
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public TPoint[] getBody() {
        return body;
    }

    public int[] getSkirt() {
        return skirt;
    }

    public Piece computeNextRotation() {
        TPoint[] result = new TPoint[4];
        for (int i = 0; i < 4; i++) {
            result[i] = new TPoint(0, 0);
            result[i].x = height - 1 + body[i].y * (-1);
            result[i].y = body[i].x;
        }
        Piece res = new Piece(result);
        return res;
    }

    public Piece fastRotation() {
        return next;
    }

    public boolean equals(Object obj) {
        // standard equals() technique 1
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof Piece)) {
            return false;
        }
        Piece other = (Piece) obj;
        int[] check = new int[4];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < this.body.length; j++) {
                if (this.body[i].equals(other.body[j])) {
                    check[i] = 1;
                    break;
                }
            }
        }
        for (int i = 0; i < body.length; i++) {
            if (check[i] != 1) {
                return false;
            }
        }
        return true;
    }
    public static final String STICK_STR = "0 0	0 1	 0 2  0 3";
    public static final String L1_STR = "0 0	0 1	 0 2  1 0";
    public static final String L2_STR = "0 0	1 0 1 1	 1 2";
    public static final String S1_STR = "0 0	1 0	 1 1  2 1";
    public static final String S2_STR = "0 1	1 1  1 0  2 0";
    public static final String SQUARE_STR = "0 0  0 1  1 0  1 1";
    public static final String PYRAMID_STR = "0 0  1 0  1 1  2 0";
    public static final int STICK = 0;
    public static final int L1 = 1;
    public static final int L2 = 2;
    public static final int S1 = 3;
    public static final int S2 = 4;
    public static final int SQUARE = 5;
    public static final int PYRAMID = 6;

    public static Piece[] getPieces() {

        if (Piece.pieces == null) {

            Piece.pieces = new Piece[]{
                makeFastRotations(new Piece(STICK_STR)),
                makeFastRotations(new Piece(L1_STR)),
                makeFastRotations(new Piece(L2_STR)),
                makeFastRotations(new Piece(S1_STR)),
                makeFastRotations(new Piece(S2_STR)),
                makeFastRotations(new Piece(SQUARE_STR)),
                makeFastRotations(new Piece(PYRAMID_STR)),};
        }


        return Piece.pieces;
    }

    private static Piece makeFastRotations(Piece root) {
        Piece root1 = root.computeNextRotation();
        if (root1.equals(root)==true) {
            root.next = root;
        } else {
            root.next = root1;
            Piece root2 = root1.computeNextRotation();
            if (root2.equals(root)==true) {
                root1.next = root;
            } else {
                root1.next = root2;
                Piece root3 = root2.computeNextRotation();
                root2.next = root3;
                root3.next = root;
            }
        }
        return root;
    }

    private static TPoint[] parsePoints(String string) {
        List<TPoint> points = new ArrayList<TPoint>();
        StringTokenizer tok = new StringTokenizer(string);
        try {
            while (tok.hasMoreTokens()) {
                int x = Integer.parseInt(tok.nextToken());
                int y = Integer.parseInt(tok.nextToken());

                points.add(new TPoint(x, y));
            }
        } catch (NumberFormatException e) {
            throw new RuntimeException("Could not parse x,y string:" + string);
        }

        TPoint[] array = points.toArray(new TPoint[0]);
        return array;
    }
}
