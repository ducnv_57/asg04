package oop.asg04;

import static org.junit.Assert.*;
import java.util.*;

import org.junit.*;

public class BoardTest{
        Board b;
        Piece pyr1, pyr2, pyr3, pyr4, s, sRotated;

        // This shows how to build things in setUp() to re-use
        // across tests.
        
        // In this case, setUp() makes shapes,
        // and also a 3X6 board, with pyr placed at the bottom,
        // ready to be used by tests.
        @Before
        public void setUp() throws Exception {
                b = new Board(3, 6);
                
                pyr1 = new Piece(Piece.PYRAMID_STR);
                pyr2 = pyr1.computeNextRotation();
                pyr3 = pyr2.computeNextRotation();
                pyr4 = pyr3.computeNextRotation();
                
                s = new Piece(Piece.S1_STR);
                sRotated = s.computeNextRotation();
        }
        
        @Test
        public void outOfBoundsTest() {
                b.commit();
                
                int result = b.place(pyr1, -1, 0);
                assertEquals(Board.PLACE_OUT_BOUNDS, result);
                
                result = b.place(pyr1, 1, 0);
                assertEquals(Board.PLACE_OUT_BOUNDS, result);
                
                result = b.place(pyr1, 0, -1);
                assertEquals(Board.PLACE_OUT_BOUNDS, result);
                
                result = b.place(pyr1, 0, 5);
                assertEquals(Board.PLACE_OUT_BOUNDS, result);
        }
        
        // Check the basic width/height/max after the one placement
        @Test
        public void testSample1() {
                b.place(pyr1, 0, 0);
                
                assertEquals(1, b.getColumnHeight(0));
                assertEquals(2, b.getColumnHeight(1));
                assertEquals(2, b.getMaxHeight());
                assertEquals(3, b.getRowWidth(0));
                assertEquals(1, b.getRowWidth(1));
                assertEquals(0, b.getRowWidth(2));
                
        }
        
        // Place sRotated into the board, then check some measures
        @Test
        public void testSample2() {
                b.place(pyr1, 0, 0);
                
                b.commit();
                int result = b.place(sRotated, 1, 1);
                assertEquals(Board.PLACE_OK, result);
                assertEquals(1, b.getColumnHeight(0));
                assertEquals(4, b.getColumnHeight(1));
                assertEquals(3, b.getColumnHeight(2));
                assertEquals(4, b.getMaxHeight());
        }
        
        // Make  more tests, by putting together longer series of 
        // place, clearRows, undo, place ... checking a few col/row/max
        // numbers that the board looks right after the operations.
        
        @Test
        public void clearRowsTest() {
                b.place(pyr1, 0, 0);
                b.clearRows();
                
                assertEquals(0, b.getColumnHeight(0));
                assertEquals(1, b.getColumnHeight(1));
                assertEquals(0, b.getColumnHeight(2));
                assertEquals(1, b.getMaxHeight());
                
        }
        
        @Test
        public void placeBadTest() {
                b.place(pyr1, 0, 0);
                b.commit();
                
                int result = b.place(sRotated, 0,0);
                assertEquals(Board.PLACE_BAD, result);
                
                result = b.place(sRotated, 1,0);
                assertEquals(Board.PLACE_BAD, result);
                
                result = b.place(sRotated, 0,1);
                assertEquals(Board.PLACE_BAD, result);
                
        }
        
        @Test
        public void dropHeightTest() {
                b = new Board(6, 6);
                b.place(pyr1, 2, 0);
                b.commit();
                
                int height = b.dropHeight(sRotated, 0);
                assertEquals(0, height);
                
                height = b.dropHeight(sRotated, 1);
                assertEquals(1, height);
                
                height = b.dropHeight(sRotated, 2);
                assertEquals(2, height);
                
                height = b.dropHeight(sRotated, 3);
                assertEquals(1, height);
                
                height = b.dropHeight(sRotated, 4);
                assertEquals(0, height);
                
                b.place(sRotated, 4, 0);
                b.commit();
                
                height = b.dropHeight(s, 0);
                assertEquals(0, height);
                
                height = b.dropHeight(s, 1);
                assertEquals(1, height);
                
                height = b.dropHeight(s, 2);
                assertEquals(2, height);
                
                height = b.dropHeight(s, 3);
                assertEquals(3, height);
                
        }
        
        @Test
        public void maxHeightTest() {
                b = new Board(6, 6);
                
                int maxHeight = b.getMaxHeight();
                assertEquals(0, maxHeight);
                
                b.place(pyr1, 2, 0);
                b.commit();
                maxHeight = b.getMaxHeight();
                assertEquals(2, maxHeight);
                
                b.place(pyr2, 0, 0);
                b.commit();
                maxHeight = b.getMaxHeight();
                assertEquals(3, maxHeight);
                
                b.place(pyr2, 4, 0);
                b.commit();
                maxHeight = b.getMaxHeight();
                assertEquals(3, maxHeight);
                
                b.place(pyr2, 4, 3);
                b.commit();
                maxHeight = b.getMaxHeight();
                assertEquals(6, maxHeight);
                
                b.place(pyr2, 0, 0);
                b.commit();
                maxHeight = b.getMaxHeight();
                assertEquals(6, maxHeight);
                
                b.place(pyr1, 0, 3);
                b.commit();
                maxHeight = b.getMaxHeight();
                assertEquals(6, maxHeight);
                
                b.place(pyr1, 0, 5);
                b.commit();
                maxHeight = b.getMaxHeight();
                assertEquals(6, maxHeight);
        }
        
        @Test
        public void undoTest() {;
                b.place(pyr1, 0, 0);
                
                assertEquals(1, b.getColumnHeight(0));
                assertEquals(2, b.getColumnHeight(1));
                assertEquals(1, b.getColumnHeight(2));
                assertEquals(2, b.getMaxHeight());
                
                b.undo();
                
                assertEquals(0, b.getColumnHeight(0));
                assertEquals(0, b.getColumnHeight(1));
                assertEquals(0, b.getColumnHeight(2));
                assertEquals(0, b.getMaxHeight());
                
                b.place(pyr1, 0, 0);
                b.clearRows();
                
                assertEquals(0, b.getColumnHeight(0));
                assertEquals(1, b.getColumnHeight(1));
                assertEquals(0, b.getColumnHeight(2));
                assertEquals(1, b.getMaxHeight());
                
                b.undo();
                
                assertEquals(0, b.getColumnHeight(0));
                assertEquals(0, b.getColumnHeight(1));
                assertEquals(0, b.getColumnHeight(2));
                assertEquals(0, b.getMaxHeight());
                
                b.place(pyr1, 0, 0);
                b.commit();
                
                b.place(sRotated, 1, 1);
                
                assertEquals(1, b.getColumnHeight(0));
                assertEquals(4, b.getColumnHeight(1));
                assertEquals(3, b.getColumnHeight(2));
                assertEquals(4, b.getMaxHeight());
                
                b.undo();
                
                assertEquals(1, b.getColumnHeight(0));
                assertEquals(2, b.getColumnHeight(1));
                assertEquals(1, b.getColumnHeight(2));
                assertEquals(2, b.getMaxHeight());
                
                b.undo();
                
                assertEquals(1, b.getColumnHeight(0));
                assertEquals(2, b.getColumnHeight(1));
                assertEquals(1, b.getColumnHeight(2));
                assertEquals(2, b.getMaxHeight());
        }
        
        @Test
        public void basicRun() {
                b = new Board(6,10);
                b.place(pyr1, 0, 0);
                
                b.commit();
                int result = b.place(sRotated, 1, 1);
                assertEquals(Board.PLACE_OK, result);
                assertEquals(1, b.getColumnHeight(0));
                assertEquals(4, b.getColumnHeight(1));
                assertEquals(3, b.getColumnHeight(2));
                assertEquals(4, b.getMaxHeight());
        }
        
}