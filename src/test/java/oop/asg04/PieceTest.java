package oop.asg04;

import static org.junit.Assert.*;
import java.util.*;

import org.junit.*;

/*
 Unit test for Piece class -- starter shell.
 M� kh?i ??u cho unit test d�nh cho l?p Piece
 */
public class PieceTest {
    // You can create data to be used in the your
    // test cases like this. For each run of a test method,
    // a new PieceTest object is created and setUp() is called
    // automatically by JUnit.
    // For example, the code below sets up some
    // pyramid and s pieces in instance variables
    // that can be used in tests.

    // B?n c� th? t?o d? li?u ?? d�ng trong c�c 
    // testcase nh? th? n�y. M?i l?n ch?y m?t ph??ng th?c test,
    // JUnit t? ??ng t?o m?t ??i t??ng PieceTest 
    // v� g?i setUp().
    // V� d?, ?o?n m� d??i ?�y thi?t l?p (setup) 
    // c�c m?nh pyramid v� S trong c�c bi?n th?c th?
    // m� c� th? s? d?ng trong c�c test
    private Piece pyr1, pyr2, pyr3, pyr4;
    private Piece s, sRotated;
    private Piece stick1, stick2;
    private Piece l11, l12, l13, l14;
    private Piece l21, l22, l23, l24;
    private Piece s11, s12;
    private Piece s21, s22;
    private Piece square;
    private Piece[] arrayOfGetPieces;

    @Before
    public void setUp() throws Exception {

        pyr1 = new Piece(Piece.PYRAMID_STR);
        pyr2 = pyr1.computeNextRotation();
        pyr3 = pyr2.computeNextRotation();
        pyr4 = pyr3.computeNextRotation();

        s = new Piece(Piece.S1_STR);
        sRotated = s.computeNextRotation();
    }

    // Here are some sample tests to get you started
    // D??i ?�y l� m?t s? test m?u ?? b?n x�y d?ng ti?p
    @Test
    public void testSampleSize() {
        // Check size of pyr piece
        // Ki?m tra k�ch th??c m?nh pyramid
        assertEquals(3, pyr1.getWidth());
        assertEquals(2, pyr1.getHeight());

        // Now try after rotation
        // Effectively we're testing size and rotation code here
        // B�y gi? th? l?i sau khi xoay
        // ? ?�y ta ?ang test m� xoay m?nh c?ng nh? k�ch th??c.
        assertEquals(2, pyr2.getWidth());
        assertEquals(3, pyr2.getHeight());

        // Now try with some other piece, made a different way
        // Th? m?t m?nh kh�c
        Piece l = new Piece(Piece.STICK_STR);
        assertEquals(1, l.getWidth());
        assertEquals(4, l.getHeight());
    }

    // Test the skirt returned by a few pieces
    // Test skirt c?a c�c m?nh
    @Test
    public void testSampleSkirt() {
        // Note must use assertTrue(Arrays.equals(... 
        // as plain .equals does not work right for arrays.
        // L?u �, ph?i d�ng assertTrue(Arrays.equals(... 
        // v� .equals() kh�ng so s�nh ???c n?i dung m?ng
        assertTrue(Arrays.equals(new int[]{0, 0, 0}, pyr1.getSkirt()));
        assertTrue(Arrays.equals(new int[]{1, 0, 1}, pyr3.getSkirt()));

        assertTrue(Arrays.equals(new int[]{0, 0, 1}, s.getSkirt()));
        assertTrue(Arrays.equals(new int[]{1, 0}, sRotated.getSkirt()));
    }

    public void testSampleSizeAndSkirt() {
        stick1 = new Piece(Piece.STICK_STR);
        stick2 = stick1.computeNextRotation();

        l11 = new Piece(Piece.L1_STR);
        l12 = l11.computeNextRotation();
        l13 = l12.computeNextRotation();
        l14 = l13.computeNextRotation();

        l21 = new Piece(Piece.L2_STR);
        l22 = l21.computeNextRotation();
        l23 = l22.computeNextRotation();
        l24 = l23.computeNextRotation();

        s11 = new Piece(Piece.S1_STR);
        s12 = s11.computeNextRotation();

        s21 = new Piece(Piece.S2_STR);
        s22 = s21.computeNextRotation();

        square = new Piece(Piece.SQUARE_STR);

        pyr1 = new Piece(Piece.PYRAMID_STR);
        pyr2 = pyr1.computeNextRotation();
        pyr3 = pyr2.computeNextRotation();
        pyr4 = pyr3.computeNextRotation();

        arrayOfGetPieces = Piece.getPieces();
        //Stick part
        assertEquals(1, stick1.getWidth());
        assertEquals(4, stick1.getHeight());
        assertEquals(4, stick2.getWidth());
        assertEquals(1, stick2.getHeight());
        assertTrue(Arrays.equals(new int[]{0}, stick1.getSkirt()));
        assertTrue(Arrays.equals(new int[]{0, 0, 0, 0}, stick2.getSkirt()));
        //L1 part
        assertEquals(2, l11.getWidth());
        assertEquals(3, l11.getHeight());
        assertEquals(3, l12.getWidth());
        assertEquals(2, l12.getHeight());
        assertEquals(2, l13.getWidth());
        assertEquals(3, l13.getHeight());
        assertEquals(3, l14.getWidth());
        assertEquals(2, l14.getHeight());
        assertTrue(Arrays.equals(new int[]{0, 0}, l11.getSkirt()));
        assertTrue(Arrays.equals(new int[]{0, 0, 0}, l12.getSkirt()));
        assertTrue(Arrays.equals(new int[]{2, 0}, l13.getSkirt()));
        assertTrue(Arrays.equals(new int[]{0, 1, 1}, l14.getSkirt()));
        //L2 part
        assertEquals(2, l21.getWidth());
        assertEquals(3, l21.getHeight());
        assertEquals(3, l22.getWidth());
        assertEquals(2, l22.getHeight());
        assertEquals(2, l23.getWidth());
        assertEquals(3, l23.getHeight());
        assertEquals(3, l24.getWidth());
        assertEquals(2, l24.getHeight());
        assertTrue(Arrays.equals(new int[]{0, 0}, l21.getSkirt()));
        assertTrue(Arrays.equals(new int[]{1, 1, 0}, l22.getSkirt()));
        assertTrue(Arrays.equals(new int[]{0, 2}, l23.getSkirt()));
        assertTrue(Arrays.equals(new int[]{0, 0, 0}, l24.getSkirt()));
        //S1 part
        assertEquals(3, s11.getWidth());
        assertEquals(2, s11.getHeight());
        assertEquals(2, s12.getWidth());
        assertEquals(3, s12.getHeight());
        assertTrue(Arrays.equals(new int[]{0, 0, 1}, s11.getSkirt()));
        assertTrue(Arrays.equals(new int[]{1, 0}, s12.getSkirt()));
        //S2 part
        assertEquals(3, s21.getWidth());
        assertEquals(2, s21.getHeight());
        assertEquals(2, s22.getWidth());
        assertEquals(3, s22.getHeight());
        assertTrue(Arrays.equals(new int[]{1, 0, 0}, s21.getSkirt()));
        assertTrue(Arrays.equals(new int[]{0, 1}, s22.getSkirt()));
        //Square part
        assertEquals(2, square.getWidth());
        assertEquals(2, square.getHeight());
        assertTrue(Arrays.equals(new int[]{0, 0}, square.getSkirt()));
        //Pyramid part
        assertEquals(3, pyr1.getWidth());
        assertEquals(2, pyr1.getHeight());
        assertEquals(2, pyr2.getWidth());
        assertEquals(3, pyr2.getHeight());
        assertEquals(3, pyr3.getWidth());
        assertEquals(2, pyr3.getHeight());
        assertEquals(2, pyr4.getWidth());
        assertEquals(3, pyr4.getHeight());
        assertTrue(Arrays.equals(new int[]{0, 0, 0}, pyr1.getSkirt()));
        assertTrue(Arrays.equals(new int[]{1, 0}, pyr2.getSkirt()));
        assertTrue(Arrays.equals(new int[]{1, 0, 1}, pyr3.getSkirt()));
        assertTrue(Arrays.equals(new int[]{0, 1}, pyr4.getSkirt()));

    }

    @Test
    public void testSampleGetPiecesAndEquals() {
        stick1 = new Piece(Piece.STICK_STR);
        stick2 = stick1.computeNextRotation();

        l11 = new Piece(Piece.L1_STR);
        l12 = l11.computeNextRotation();
        l13 = l12.computeNextRotation();
        l14 = l13.computeNextRotation();

        l21 = new Piece(Piece.L2_STR);
        l22 = l21.computeNextRotation();
        l23 = l22.computeNextRotation();
        l24 = l23.computeNextRotation();

        s11 = new Piece(Piece.S1_STR);
        s12 = s11.computeNextRotation();

        s21 = new Piece(Piece.S2_STR);
        s22 = s21.computeNextRotation();

        square = new Piece(Piece.SQUARE_STR);

        pyr1 = new Piece(Piece.PYRAMID_STR);
        pyr2 = pyr1.computeNextRotation();
        pyr3 = pyr2.computeNextRotation();
        pyr4 = pyr3.computeNextRotation();

        arrayOfGetPieces = Piece.getPieces();
        assertTrue(stick1.equals(arrayOfGetPieces[ 0]));
        assertTrue(stick2.equals(arrayOfGetPieces[ 0].fastRotation()));
        assertTrue(stick1.equals(arrayOfGetPieces[ 0].fastRotation().fastRotation()));
        assertTrue(l11.equals(arrayOfGetPieces[ 1]));
        assertTrue(l12.equals(arrayOfGetPieces[ 1].fastRotation()));
        assertTrue(l13.equals(arrayOfGetPieces[ 1].fastRotation().fastRotation()));
        assertTrue(l14.equals(arrayOfGetPieces[ 1].fastRotation().fastRotation().fastRotation()));
        assertTrue(l11.equals(arrayOfGetPieces[ 1].fastRotation().fastRotation().fastRotation().fastRotation()));
        assertTrue(l21.equals(arrayOfGetPieces[ 2]));
        assertTrue(l22.equals(arrayOfGetPieces[ 2].fastRotation()));
        assertTrue(l23.equals(arrayOfGetPieces[ 2].fastRotation().fastRotation()));
        assertTrue(l24.equals(arrayOfGetPieces[ 2].fastRotation().fastRotation().fastRotation()));
        assertTrue(l21.equals(arrayOfGetPieces[ 2].fastRotation().fastRotation().fastRotation().fastRotation()));
        assertTrue(s11.equals(arrayOfGetPieces[ 3]));
        assertTrue(s12.equals(arrayOfGetPieces[ 3].fastRotation()));
        assertTrue(s11.equals(arrayOfGetPieces[ 3].fastRotation().fastRotation()));
        assertTrue(s21.equals(arrayOfGetPieces[ 4]));
        assertTrue(s22.equals(arrayOfGetPieces[ 4].fastRotation()));
        assertTrue(s21.equals(arrayOfGetPieces[ 4].fastRotation().fastRotation()));
        assertTrue(square.equals(arrayOfGetPieces[ 5]));
        assertTrue(square.equals(arrayOfGetPieces[ 5].fastRotation()));
        assertTrue(pyr1.equals(arrayOfGetPieces[ 6]));
        assertTrue(pyr2.equals(arrayOfGetPieces[ 6].fastRotation()));
        assertTrue(pyr3.equals(arrayOfGetPieces[ 6].fastRotation().fastRotation()));
        assertTrue(pyr4.equals(arrayOfGetPieces[ 6].fastRotation().fastRotation().fastRotation()));
        assertTrue(pyr1.equals(arrayOfGetPieces[ 6].fastRotation().fastRotation().fastRotation().fastRotation()));
    }

    @Test
    public void testSampleGetPiecesAndEquals2() {
        stick1 = new Piece(Piece.STICK_STR);
        stick2 = stick1.computeNextRotation();

        l11 = new Piece(Piece.L1_STR);
        l12 = l11.computeNextRotation();
        l13 = l12.computeNextRotation();
        l14 = l13.computeNextRotation();

        l21 = new Piece(Piece.L2_STR);
        l22 = l21.computeNextRotation();
        l23 = l22.computeNextRotation();
        l24 = l23.computeNextRotation();

        s11 = new Piece(Piece.S1_STR);
        s12 = s11.computeNextRotation();

        s21 = new Piece(Piece.S2_STR);
        s22 = s21.computeNextRotation();

        square = new Piece(Piece.SQUARE_STR);

        pyr1 = new Piece(Piece.PYRAMID_STR);
        pyr2 = pyr1.computeNextRotation();
        pyr3 = pyr2.computeNextRotation();
        pyr4 = pyr3.computeNextRotation();

        arrayOfGetPieces = Piece.getPieces();
        assertFalse(l11.equals(l21));
        assertFalse(l12.equals(l24));
        assertFalse(l24.equals(pyr1));
        assertFalse(pyr1.equals(l12));
        assertFalse(l24.equals(pyr1));
        assertFalse(s12.equals(pyr2));
        assertFalse(pyr4.equals(s22));
    }
}